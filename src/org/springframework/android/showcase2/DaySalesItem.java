package org.springframework.android.showcase2;

import org.simpleframework.xml.Element;

public class DaySalesItem{
    @Element
    protected int day;
    @Element
    protected int numberOfSales;
    @Element
    protected float totalOfSales;

    public DaySalesItem(int day, int numberOfSales, float totalOfSales) {
        this.day = day;
        this.numberOfSales = numberOfSales;
        this.totalOfSales = totalOfSales;
    }

    public DaySalesItem(){

    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getNumberOfSales() {
        return numberOfSales;
    }

    public void setNumberOfSales(int numberOfSales) {
        this.numberOfSales = numberOfSales;
    }

    public float getTotalOfSales() {
        return totalOfSales;
    }

    public void setTotalOfSales(float totalOfSales) {
        this.totalOfSales = totalOfSales;
    }
}