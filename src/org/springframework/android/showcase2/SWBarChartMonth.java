package org.springframework.android.showcase2;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.Display;
import android.view.WindowManager;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by johnadmin on 2013/08/23.
 */
public class SWBarChartMonth {

    protected Context activityContext;
    private int clientWindowWidth;
    private int clientWindowHeight;
    private float barWidth;
    private float barSpacing;
    private String[] xAxisLabels;

    public int getClientWindowHeight() {
        return clientWindowHeight;
    }

    public int getClientWindowWidth() {
        return clientWindowWidth;
    }

    public SWBarChartMonth(Context context){
        this.activityContext = context;

        Display display =  ((WindowManager)context.getSystemService(context.WINDOW_SERVICE)).getDefaultDisplay();
        this.clientWindowWidth = display.getWidth();
        this.clientWindowHeight = display.getHeight();
        this.barSpacing = 2f;
    }

    public GraphicalView initChart(float[] randValues){
        String[] titles = new String[] { "XYZ" };

        List<float[]> values = new ArrayList<float[]>();

        values.add(randValues);

        xAxisLabels = new String[randValues.length];

        for(int i = 0; i < randValues.length; i++){
            xAxisLabels[i] = String.valueOf(i+1);
        }

        int[] colors = new int[] { Color.rgb(246, 152, 0) };

        this.barWidth = (clientWindowWidth / randValues.length) - this.barSpacing - 2f;

        float calculatedChartWidth = randValues.length * (this.barWidth + this.barSpacing);

        XYMultipleSeriesRenderer bargraphrenderer = this.buildBarRenderer(colors);
        float xMax = randValues.length + 0.1f;
        setChartSettings(bargraphrenderer, "Daily sales", "Day", "Rand", 0, xMax, 0.000f, this.findHigestYValue(randValues), Color.WHITE, Color.rgb(255, 203, 22));
        bargraphrenderer.setXLabels(randValues.length);
        bargraphrenderer.setApplyBackgroundColor(false);
        bargraphrenderer.setBackgroundColor(Color.parseColor("#000000"));
        bargraphrenderer.setYLabels(10);
        bargraphrenderer.setXLabelsAlign(Paint.Align.CENTER);
        bargraphrenderer.setYLabelsAlign(Paint.Align.LEFT);
        bargraphrenderer.setBarSpacing(this.barSpacing);
        bargraphrenderer.setBarWidth(this.barWidth * 0.90f);
        bargraphrenderer.setXLabels(0);
        bargraphrenderer.setShowGrid(true);
        bargraphrenderer.setShowGridX(true);
        bargraphrenderer.setShowGridY(true);
        bargraphrenderer.setPanEnabled(false);
        bargraphrenderer.setXAxisMin(-0.5);
        //bargraphrenderer.setXAxisMin();
        //bargraphrenderer.setXAxisMax(0.5);

        XYMultipleSeriesDataset barDataSet = buildBarDataset("XYZ", randValues);

        return ChartFactory.getBarChartView(activityContext, barDataSet, bargraphrenderer, BarChart.Type.DEFAULT);
    }

    private float findHigestYValue(float[] salesItemsByDay){
        float highestSaleValue = 0f;
        float currentItemValueAsFlt = 0f;

        for(int i = 0; i <= salesItemsByDay.length-1; i++){
            currentItemValueAsFlt = salesItemsByDay[i];

            if(currentItemValueAsFlt > highestSaleValue)
                highestSaleValue = currentItemValueAsFlt;
        }

        int stepVal = 10;
        final float maxSaleValuePossible = 1000000000f; // 1 Billion (Financial)

        final float[] maxValuesPossible = new float[]{  1000f, 2500f, 5000f, 7500f,
                10000f, 15000f, 20000f, 25000f, 25000f, 30000f,
                35000f, 40000f, 45000f, 50000f, 55000f, 60000f,
                65000f, 70000f, 75000f, 80000f, 85000f, 90000f,
                95000f, 100000f, 250000f, 500000f, 750000f, 1000000f,
                1000000f, 2500000f, 5000000f, 7500000f, 10000000f,
                10000000f, 25000000f, 50000000f, 75000000f, 100000000f,
        };

        for(int i = 0; i < maxValuesPossible.length; i++){
            float currentSteppedValue = maxValuesPossible[i];

            if(currentSteppedValue > highestSaleValue)
                return currentSteppedValue;
        }

        // Default to highest if failure within loop above
        return maxSaleValuePossible;
    }

    /**
     * Builds a bar multiple series renderer to use the provided colors.
     *
     * @param colors
     *            the series renderers colors
     * @return the bar multiple series renderer
     */
    protected XYMultipleSeriesRenderer buildBarRenderer(int[] colors) {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(22);
        renderer.setChartTitleTextSize(20);
        renderer.setLabelsTextSize(18);
        renderer.setLegendTextSize(15);
        renderer.setShowLegend(false);
        //renderer.setXAxisMax(this.clientWindowWidth-1);
        renderer.setXRoundedLabels(false);

        int length = colors.length;
        for (int i = 0; i < length; i++) {
            XYSeriesRenderer r = new XYSeriesRenderer();
            r.setColor(colors[i]);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    /**
     * Sets a few of the series renderer settings.
     *
     * @param renderer
     *            the renderer to set the properties to
     * @param title
     *            the chart title
     * @param xTitle
     *            the title for the X axis
     * @param yTitle
     *            the title for the Y axis
     * @param xMin
     *            the minimum value on the X axis
     * @param xMax
     *            the maximum value on the X axis
     * @param yMin
     *            the minimum value on the Y axis
     * @param yMax
     *            the maximum value on the Y axis
     * @param axesColor
     *            the axes color
     * @param labelsColor
     *            the labels color
     */
    protected void setChartSettings(XYMultipleSeriesRenderer renderer,
                                    String title, String xTitle, String yTitle, float xMin,
                                    float xMax, float yMin, float yMax, int axesColor,
                                    int labelsColor) {
        renderer.setChartTitle(title);
        renderer.setXTitle(xTitle);
        renderer.setYTitle(yTitle);
        renderer.setXAxisMin(xMin);
        renderer.setXAxisMax(xMax);
        renderer.setYAxisMin(yMin);
        renderer.setYAxisMax(yMax);
        renderer.setAxesColor(axesColor);
        renderer.setLabelsColor(labelsColor);

        for(int i = 0; i < xAxisLabels.length; i++){
            renderer.addXTextLabel(i, xAxisLabels[i]);
        }

    }

    /**
     * Builds a bar multiple series dataset using the provided values.
     *
     * @param title
     *            the series titles
     * @param values
     *            the values
     * @return the XY multiple bar dataset
     */
    protected XYMultipleSeriesDataset buildBarDataset(String title, float[] values) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

        XYSeries series = new XYSeries(title);
        series.clear();

        for (int k = 0; k < values.length; k++) {
            series.add(k, values[k]);
        }

        dataset.addSeries(series);

        return dataset;
    }
}
