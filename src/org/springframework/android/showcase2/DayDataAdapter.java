package org.springframework.android.showcase2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class DayDataAdapter
        extends ArrayAdapter<String[]> {
    private ArrayList<String[]> dData;
    private Context context;

    public DayDataAdapter(Context context, int textViewResourceId, ArrayList<String[]> dData) {
        super(context, textViewResourceId, dData);
        this.dData = dData;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_layout, null);
        }

        String[] data = dData.get(position);
        if (data != null) {
            TextView lbl = (TextView) v.findViewById(R.id.day_row_label);
            TextView val = (TextView) v.findViewById(R.id.day_row_value);
            TextView val2 = (TextView) v.findViewById(R.id.day_row_rand);

            if (lbl != null) {
                lbl.setText(data[0].toString());
            }

            if (val != null) {

                val.setText(data[1].toString());
            }

            if (val2 != null) {

                val2.setText(data[2].toString());
            }


        }
        return v;
    }
}