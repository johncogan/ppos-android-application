package org.springframework.android.showcase2;

import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.preference.PreferenceActivity;
import android.content.SharedPreferences;


/**
 * Created by johnadmin on 2013/08/14.
 */
public class SettingsActivity extends PreferenceActivity  implements SharedPreferences.OnSharedPreferenceChangeListener {

    private String hostUrl;
    private String port;
    private String passPhrase;
    private Boolean useSsl;

    private SharedPreferences prefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.myapppreferences);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPref, String key) {
        SharedPreferences.Editor editor = sharedPref.edit();

        if(key.equalsIgnoreCase("settings_input_host")){
            MainActivity.settingsHostUrl = sharedPref.getString("settings_input_host", "");
            editor.putString("settings_input_host", MainActivity.settingsHostUrl);
        }

        if(key.equalsIgnoreCase("settings_input_port")){
            MainActivity.settingsPort = sharedPref.getString("settings_input_port", "");
            editor.putString("settings_input_port", MainActivity.settingsPort);
        }

        if(key.equalsIgnoreCase("settings_input_password")){
            MainActivity.settingsPassPhrase = sharedPref.getString("settings_input_password", "");
            editor.putString("settings_input_password", MainActivity.settingsPassPhrase);
        }

        editor.commit();
    }

    public void onClickLoad(View view){
        Intent i = new Intent("org.springframework.android.showcase2.SettingsActivity");
        startActivity(i);
    }
}
