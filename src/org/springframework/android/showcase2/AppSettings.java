package org.springframework.android.showcase2;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by johnadmin on 2013/08/20.
 */
public class AppSettings {
    private SharedPreferences sharedPrefs;

    private String hostUrl;
    private String port;
    private String passPhrase;
    private Boolean useSsl;

    public AppSettings(Context context){
        //sharedPrefs = context.getSharedPreferences("myapppreferences", context.MODE_WORLD_READABLE);
        sharedPrefs = context.getSharedPreferences("myapppreferences", context.MODE_WORLD_READABLE);
        this.hostUrl = sharedPrefs.getString("settings_input_host", "");
        this.port = sharedPrefs.getString("settings_input_port", "");
        this.passPhrase = sharedPrefs.getString("settings_input_password", "");
        this.useSsl = sharedPrefs.getBoolean("checkbox_preference", false);
    }

    public AppSettings(){

    }

    public String getFullHostURI(){
        if(this.hostUrl.toLowerCase().indexOf("http://") >= 0){
            return this.hostUrl + ":" + this.port;
        }else{
            return "http://" + this.hostUrl + ":" + this.port;
        }
    }

    /*

    public String getHostUrl() {
        return hostUrl;
    }

    public String getPort() {
        return port;
    }

    public String getPassPhrase() {
        return passPhrase;
    }

    public Boolean getUseSsl() {
        return useSsl;
    }
    */
}