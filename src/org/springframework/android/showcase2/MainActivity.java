package org.springframework.android.showcase2;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.content.Intent;
import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.Spinner;
import com.crashlytics.android.Crashlytics;
import java.util.ArrayList;
import android.app.FragmentManager;

import java.util.Calendar;

import android.content.SharedPreferences;
import android.widget.Toast;


/**
 * @author John Cogan
 */
public class MainActivity extends Activity implements DatePicker.OnDateChangedListener {

    private Button mDtdButton;
    private Button mMtdButton;
    private Button mYtdButton;
    private Button mSettingsButton;
    private TextView mTViewCompIntro;
    private Button mSendRequest;
    private Spinner mCalViewType;

    private DatePicker mDatePicker;
    private FragmentManager fm;

    private Calendar selectedDateFromDatePicker;

    public SharedPreferences prefs;
    static public String settingsHostUrl;
    static public String settingsPort;
    static public String settingsPassPhrase;

    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        selectedDateFromDatePicker.set(year, monthOfYear, dayOfMonth);
    }

    public void addListenerOnSpinnerItemSelection(){

        ArrayList<String> array = new ArrayList<String>();

        array.add("Day");
        array.add("Month");
        array.add("Year");

        Spinner spinner1;

        ArrayAdapter<String> mAdapter;

        spinner1 = (Spinner) findViewById(R.id.calViewType);

        mAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, array);

        spinner1.setAdapter(mAdapter);

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);

        String versionName = null;

        fm = getFragmentManager();

        try{
            versionName = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
            Crashlytics.log(1, "PBS ANDROID VERSION", versionName);
        }
        catch(PackageManager.NameNotFoundException nnfException)
        {
            // Not implemented
        }

        TextView headerLabel = (TextView) findViewById(R.id.tViewCompanyIntro);

        //String headerText = headerLabel.getText().toString();

        if(versionName != null && headerLabel != null)
        {
            versionName = headerLabel.getText().toString() + " v"  + versionName;

            headerLabel.setText(versionName);
        }

        setContentView(R.layout.home_layout);
        initializeStyle();

        addListenerOnSpinnerItemSelection();

        //currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);

        selectedDateFromDatePicker = Calendar.getInstance();

        mSettingsButton = (Button)findViewById(R.id.home_btn_settings);
        mSendRequest = (Button)findViewById(R.id.home_btn_request);
        mCalViewType = (Spinner) findViewById(R.id.calViewType);

        mTViewCompIntro = (TextView)findViewById(R.id.tViewCompanyIntro);

        mDatePicker = (DatePicker)findViewById(R.id.datePicker);
        mDatePicker.init(cal.getTime().getYear()+1900, cal.getTime().getMonth()+1, cal.getTime().getDay(), this);
        mDatePicker.setMaxDate(cal.getTimeInMillis());

        mSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String calViewTypeSelected = "Day";
                Intent i;

                calViewTypeSelected = mCalViewType.getSelectedItem().toString();
                Log.v("calViewTypeSelected: ", calViewTypeSelected);

                int selectedYear = mDatePicker.getYear();
                int selectedMonth = mDatePicker.getMonth();
                int selectedDay = mDatePicker.getDayOfMonth();

                String dateAsString = selectedYear + "/" + selectedMonth + selectedDay;

                if(calViewTypeSelected == "Year"){
                    i = new Intent("org.springframework.android.showcase2.HttpGetYearActivity");
                }else if(calViewTypeSelected == "Month"){
                    i = new Intent("org.springframework.android.showcase2.HttpGetMonthActivity");
                }else{
                    i = new Intent("org.springframework.android.showcase2.HttpGetDayActivity");
                }

                i.putExtra("SELECTED_DATE_YEAR", selectedYear);
                i.putExtra("SELECTED_DATE_MONTH", selectedMonth);
                i.putExtra("SELECTED_DATE_DAY", selectedDay);

                initializeStyle();

                if(checkSettings())
                {
                    startActivity(i);
                }else{
                    displayErrorModal("Invalid settings", "Please ensure you have provided all the correct information in the settings.");
                }
            }
        });


        mSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Intent i = new Intent("org.springframework.android.showcase2.SettingsActivity");
                    startActivity(i);
                }
                catch(ActivityNotFoundException anfException)
                {
                    displayErrorModal("Settings activity not found error", "An error occured when trying to open the settings. Please retry and if it occurs again please notify support.");
                    Log.v("ActivityNotFoundException - Message", anfException.getMessage());
                    Log.v("ActivityNotFoundException - StackTrace", anfException.getStackTrace().toString());
                }
                catch(Exception generalException)
                {
                    // Not implemented
                }
            }
        });

        //throw new RuntimeException("This is a crash");

    }

    private boolean checkSettings(){
        if(settingsHostUrl.length() <= 0 || settingsPort.length() <= 0 || settingsPassPhrase.length() <= 0)
        {
            return false;
        }
        else{
            return true;
        }
    }

    private void displayErrorModal(String title, String message){
        final MyAlertDialogFragment testDialog = new MyAlertDialogFragment(title, message, false);

        testDialog.setRetainInstance(true);

        testDialog.show(fm, "fragment_name");

    }

    private void initializeStyle() {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        settingsHostUrl = prefs.getString("settings_input_host", "");
        settingsPort = prefs.getString("settings_input_port", "");
        settingsPassPhrase = prefs.getString("settings_input_password", "");
    }
}
