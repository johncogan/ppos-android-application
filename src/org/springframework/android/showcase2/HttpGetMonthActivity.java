package org.springframework.android.showcase2;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.achartengine.GraphicalView;
import android.view.ViewGroup.LayoutParams;
/**
 * Created by johnadmin on 2013/08/29.
 */
public class HttpGetMonthActivity extends AbstractAsyncActivity {

    private TextView day_tView_Header;
    private android.widget.ListView day_lView_TableData;
    private LinearLayout day_chartlayout_ChartData;

    private Date selectedDate;
    private Date currentDate;
    private Date nextDate;
    private Date prevDate;
    private SimpleDateFormat ft;
    private MonthData dataMonth;
    private AppSettings aSettings;

    private GraphicalView mBarChartView;
    private SWBarChartMonth swBarChart;

    private FragmentManager fm;
    private boolean useTestDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        useTestDate = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_layout);

        int selectedYearValue = 0;
        int selectedMonthValue = 0;
        int selectedDayValue = 0;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            selectedYearValue = extras.getInt("SELECTED_DATE_YEAR");
            selectedMonthValue = extras.getInt("SELECTED_DATE_MONTH");
            selectedDayValue = extras.getInt("SELECTED_DATE_DAY");
        }

        ft = new SimpleDateFormat ("yyyy-MM-dd");
        fm = getFragmentManager();

        day_lView_TableData = (ListView) findViewById(R.id.day_lView_TableData);
        day_chartlayout_ChartData = (LinearLayout) findViewById(R.id.chart);

        day_tView_Header = (TextView) findViewById(R.id.day_tView_Header);

        Calendar cal = Calendar.getInstance();

        cal.set(selectedYearValue, selectedMonthValue, selectedDayValue);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

        try{
            String calend = formatter.format(cal.getTime());
            currentDate = formatter.parse(calend);
        }catch(Exception ex){
            Log.e(TAG, ex.getMessage());
        }

        selectedDate = currentDate;
        day_tView_Header.setText(new SimpleDateFormat("MMM yyyy").format(currentDate));
        new PostMessageTask().execute();


        /*
            Toggle between tabular and graph views
         */
        final Button buttonToggleView = (Button) findViewById(R.id.day_btn_viewtype);
        final LinearLayout subheaders = (LinearLayout) findViewById(R.id.subheaders);
        final TextView subhead2 = (TextView) findViewById(R.id.day_tView_Header2);
        final TextView subhead3 = (TextView) findViewById(R.id.day_tView_Header3);
        final TextView subhead4 = (TextView) findViewById(R.id.day_tView_Header4);

        buttonToggleView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(day_lView_TableData.getVisibility() == View.VISIBLE)
                {
                    displayGraphView();
                    buttonToggleView.setText("Detail");
                    subhead2.setVisibility(View.INVISIBLE);
                    subhead3.setVisibility(View.INVISIBLE);
                    subhead4.setVisibility(View.INVISIBLE);

                }
                else{
                    displayTabularDataView();
                    buttonToggleView.setText("Graph");
                    subhead2.setVisibility(View.VISIBLE);
                    subhead3.setVisibility(View.VISIBLE);
                    subhead4.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public void onClickLoad(View view){
        Intent i = new Intent("org.springframework.android.showcase2.HttpGetMonthActivity");
        startActivity(i);
    }

    private void showResult(MonthData result) {
        displayTabularDataView();

        swBarChart = new SWBarChartMonth(this);

        SimpleDateFormat gregDateFormatter = new SimpleDateFormat("MMM yyyy");
        SimpleDateFormat slashDateFormatter = new SimpleDateFormat("yyyy/MM/dd");

        day_tView_Header.setText("Monthly sales for " + gregDateFormatter.format(selectedDate));

        if (result != null) {
            String errorMsg = "Error Msg: " + result.error;
            String errorTitle = "Exception - NullPointer";

            if(result.error != null){
                if(result.error.equalsIgnoreCase("nopassphrase")){
                    errorMsg = "Please provide a password - Please use the back button after closing this message and then provide the password in the settings.";
                    errorTitle = "No password";
                    displayErrorModal(errorTitle, errorMsg, true);
                }

                if(result.error.equalsIgnoreCase("invalidpassphrase")){
                    errorMsg = "Provided password is incorrect - Please use the back button after closing this message and then correct the password in the settings.";
                    errorTitle = "Invalid password";
                    displayErrorModal(errorTitle, errorMsg, true);
                }

                if(result.error.equalsIgnoreCase("invaliddate")){
                    errorMsg = "The date chosen is invalid - Please use the back button after closing this message";
                    errorTitle = "Invalid date";
                    displayErrorModal(errorTitle, errorMsg, true);
                }

                if(result.error.equalsIgnoreCase("serverexception")){
                    errorMsg = "An exception occured on the server - Please use the back button after closing this message";
                    errorTitle = "General exception";
                    displayErrorModal(errorTitle, errorMsg, true);
                }

                if(result.error.equalsIgnoreCase("nodata")){
                    errorMsg = "No data available for the selected date";
                    errorTitle = "No data for selected date";
                    displayErrorModal(errorTitle, errorMsg, true);
                }
            }

            try{
                selectedDate = slashDateFormatter.parse(result.getTargetDate());
                prevDate = slashDateFormatter.parse(result.getPreviousDate());
                nextDate = slashDateFormatter.parse(result.getNextDate());
            }catch(Exception ex){
                // displayErrorModal("Exception - NullPointer", "Server Message: " + result.error);



            }

            dataMonth = result;

            try{
                MonthDataAdapter aa = new MonthDataAdapter(this, R.layout.row_layout, buildBasicDataArrayForListView(result));
                aa.notifyDataSetChanged();
                day_lView_TableData.setAdapter(aa);

                float[] dblRandValues = new float[Integer.parseInt(result.numDaysInMonth)];
                List<DaySalesItem> hsItems = null;

                try{
                    hsItems = result.getMonthlySalesFigures();
                }catch(java.lang.NullPointerException npEx){
                    Log.e(TAG, npEx.getMessage());
                }

                for(int i = 0; i <= dblRandValues.length-1; i++){
                    DaySalesItem hsItem = hsItems.get(i);
                    dblRandValues[i] = hsItem.getTotalOfSales();
                }

                mBarChartView = swBarChart.initChart(dblRandValues);

                LinearLayout layoutBarGraph = (LinearLayout) findViewById(R.id.chart);
                layoutBarGraph.removeAllViews();
                layoutBarGraph.addView(mBarChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
                day_chartlayout_ChartData.refreshDrawableState();
                mBarChartView.repaint();
            }catch(Exception generalEx){

            }
        } else {
            displayErrorModal("Server message", "No data returned for date: " + ft.format(selectedDate), true);
        }


    }

    private ArrayList<String[]> buildBasicDataArrayForListView(MonthData dData){

        ArrayList<String[]> arr = new ArrayList<String[]>();


        String tmpVal = "R " + formatDecimal(dData.getTotalCash());
        arr.add(new String[]{"Cash", Integer.toString(dData.getNumberOfCashSales()), tmpVal});
        //arr.add(new String[]{"Cash Total", "R" + formatDecimal(dData.getTotalCash())});

        tmpVal = "R " + formatDecimal(dData.getTotalCard());
        arr.add(new String[]{"Card", Integer.toString(dData.getNumberOfCardSales()), tmpVal});
        //arr.add(new String[]{"Card Total", "R" + formatDecimal(dData.getTotalCard())});

        tmpVal = "R " + formatDecimal(dData.getTotalCheque());
        arr.add(new String[]{"Cheque", Integer.toString(dData.getNumberOfChequeSales()), tmpVal});
        //arr.add(new String[]{"Cheque Total", "R" + formatDecimal(dData.getTotalCheque())});

        tmpVal = "R " + formatDecimal(dData.getTotalLoyalty());
        arr.add(new String[]{"Loyalty", Integer.toString(dData.getNumberOfLoyaltySales()), tmpVal});
        //arr.add(new String[]{"Loyalty Total", "R" + formatDecimal(dData.getTotalLoyalty())});

        tmpVal = "R " + formatDecimal(dData.getTotalCharge());
        arr.add(new String[]{"Charge", Integer.toString(dData.getNumberOfChargeSales()), tmpVal});
        //arr.add(new String[]{"Charge Total", "R" + formatDecimal(dData.getTotalCharge())});

        tmpVal = "R " + formatDecimal(dData.getTotalGrand());
        arr.add(new String[]{" ", " " , " "});
        arr.add(new String[]{"Grand Total", " ", tmpVal});

        /*
        arr.add(" ");
        arr.add("Grand Total: " + formatDecimal(dData.getTotalGrand()));
*/
        return arr;
    }



    private String formatDecimal(float number) {
        //return String.format("%10.2f", number);
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        formatter.setMinimumFractionDigits(2);
        symbols.setGroupingSeparator(',');
        return formatter.format(number);
    }

    private void displayErrorModal(String title, String message, boolean goToMainActivity){
        final MyAlertDialogFragment testDialog = new MyAlertDialogFragment(title, message, goToMainActivity);

        testDialog.setRetainInstance(true);

        testDialog.show(fm, "fragment_name");

    }

    private void updateButtonDateTargets(Date currentDateSelected){
        final Calendar date = Calendar.getInstance();
        date.set(currentDateSelected.getYear() + 1900, currentDateSelected.getMonth(), currentDateSelected.getDay());
        date.add(Calendar.MONTH, 1);
        nextDate = date.getTime();
        date.add(Calendar.MONTH, -2);
        prevDate = date.getTime();
    }

    private void displayErrorView(){
        day_lView_TableData.setVisibility(View.GONE);
        day_chartlayout_ChartData.setVisibility(View.GONE);
    }

    private void displayTabularDataView(){
        day_lView_TableData.setVisibility(View.VISIBLE);
        day_chartlayout_ChartData.setVisibility(View.GONE);
    }

    private void displayGraphView(){
        day_lView_TableData.setVisibility(View.GONE);
        day_chartlayout_ChartData.setVisibility(View.VISIBLE);
    }

    // ***************************************
    // Private class
    // ***************************************
    private class ToggleDayView {

    }

    // ***************************************
    // Private class
    // ***************************************
    private class PostMessageTask extends AsyncTask<Void, Void, MonthData> {
        private String abbreviation;
        private String hostUri;

        @Override
        protected void onPreExecute() {
            showLoadingProgressDialog();

            // Date format to be passed: yyyy-mm-dd
            abbreviation = ft.format(selectedDate);
        }

        @Override
        protected MonthData doInBackground(Void... params) {
            String encodedQS = "";
            encodedQS = "/Sales/Month/" + abbreviation;

            // The URL for making the GET request
            hostUri = MainActivity.settingsHostUrl + ":" + MainActivity.settingsPort + encodedQS;

            try {
                MediaType mediaType = MediaType.APPLICATION_JSON;

                // Set the Accept header for "application/json" or "application/xml"
                HttpHeaders requestHeaders = new HttpHeaders();
                List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
                acceptableMediaTypes.add(mediaType);
                requestHeaders.setAccept(acceptableMediaTypes);

                // Add the pass phrase to the header
                requestHeaders.add("p", MainActivity.settingsPassPhrase);

                // Populate the headers in an HttpEntity object to use for the request
                HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);

                // Create a new RestTemplate instance
                RestTemplate restTemplate = new RestTemplate();
                if (mediaType.equals(MediaType.APPLICATION_JSON)) {
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                } else if (mediaType.equals(MediaType.APPLICATION_XML)) {
                    restTemplate.getMessageConverters().add(new SimpleXmlHttpMessageConverter());
                }

                // Perform the HTTP GET request
                ResponseEntity<MonthData> responseEntity = restTemplate.exchange(hostUri, HttpMethod.GET, requestEntity, MonthData.class, abbreviation);

                // Return the state from the ResponseEntity
                return responseEntity.getBody();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(MonthData result) {
            dismissProgressDialog();
            showResult(result);
        }

    }
}
