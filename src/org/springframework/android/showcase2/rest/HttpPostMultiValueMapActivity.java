package org.springframework.android.showcase2.rest;

import org.springframework.android.showcase2.AbstractAsyncActivity;
import org.springframework.android.showcase2.R;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

//import android.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * @author Roy Clarkson
 */
public class HttpPostMultiValueMapActivity extends AbstractAsyncActivity {

	protected static final String TAG = HttpPostMultiValueMapActivity.class.getSimpleName();
    protected String postUrl = new String();

	// ***************************************
	// Activity methods
	// ***************************************
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.http_post_multi_value_activity_layout);

		// Initiate the JSON POST request when the JSON button is clicked
		final Button buttonJson = (Button) findViewById(R.id.button_submit);
		buttonJson.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				new PostMessageTask().execute();
			}
		});

        this.postUrl = "http://192.168.0.202:9000/Sales/Day";
	}

	// ***************************************
	// Private methods
	// ***************************************
	private void showResult(String result) {
		if (result != null) {
			// display a notification to the user with the response message
			Toast.makeText(this, result, Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(this, "I got null, something happened!", Toast.LENGTH_LONG).show();
		}
	}

	// ***************************************
	// Private classes
	// ***************************************
	private class PostMessageTask extends AsyncTask<Void, Void, String> {

		private MultiValueMap<String, String> message;

		@Override
		protected void onPreExecute() {
			showLoadingProgressDialog();

			// assemble the map
			message = new LinkedMultiValueMap<String, String>();

            EditText editText = (EditText) findViewById(R.id.edit_text_message_id);

			// message.add("id", postUrl);

			editText = (EditText) findViewById(R.id.edit_text_message_subject);
			message.add("TargetDate", editText.getText().toString());

			editText = (EditText) findViewById(R.id.edit_text_message_text);
            // message.add("ViewType", editText.getText().toString());
			message.add("ViewType", "graph");
		}

		@Override
		protected String doInBackground(Void... params) {
            String urlToPostTo = "";
            urlToPostTo = postUrl;

			try {
                EditText editText = (EditText) findViewById(R.id.edit_text_message_id);

                if(editText.getText().toString() == "day"){
                    urlToPostTo = "/Sales/Day";
                }

                if(editText.getText().toString() == "month"){
                    urlToPostTo += "/Sales/Month";
                }

                if(editText.getText().toString() == "month"){
                    urlToPostTo += "/Sales/Year";
                }

                // The URL for making the POST request
				final String url = urlToPostTo; // getString(R.string.base_uri) + "/sendmessagemap";

				// Create a new RestTemplate instance
				RestTemplate restTemplate = new RestTemplate(true);

				// Make the network request, posting the message, expecting a String in response from the server
				ResponseEntity<String> response = restTemplate.postForEntity(url, message, String.class);

				// Return the response body to display to the user
				return response.getBody();
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			dismissProgressDialog();
			showResult(result);
		}

	}

}
