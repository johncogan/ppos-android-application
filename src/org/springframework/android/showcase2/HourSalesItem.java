package org.springframework.android.showcase2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by John on 2013/08/18.
 */

public class HourSalesItem{
    @Element
    protected int hour;
    @Element
    protected int numberOfSales;
    @Element
    protected float totalOfSales;

    public HourSalesItem(int hour, int numberOfSales, float totalOfSales) {
        this.hour = hour;
        this.numberOfSales = numberOfSales;
        this.totalOfSales = totalOfSales;
    }

    public HourSalesItem(){

    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getNumberOfSales() {
        return numberOfSales;
    }

    public void setNumberOfSales(int numberOfSales) {
        this.numberOfSales = numberOfSales;
    }

    public float getTotalOfSales() {
        return totalOfSales;
    }

    public void setTotalOfSales(float totalOfSales) {
        this.totalOfSales = totalOfSales;
    }
}