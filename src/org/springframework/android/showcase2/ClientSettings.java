package org.springframework.android.showcase2;

/**
 * Created by johnadmin on 2013/08/15.
 */
public class ClientSettings {
    /*
        Class fields
     */
    protected String settingHost;
    protected String settingPort;
    protected String settingPassword;
    protected Boolean settingUseSSL;

    public void ClientSettings(){

    }

    /*
        Getters and Setters
     */
    public Boolean getSettingUseSSL() {
        return settingUseSSL;
    }

    public void setSettingUseSSL(Boolean settingUseSSL) {
        this.settingUseSSL = settingUseSSL;
    }

    public String getSettingHost() {
        return settingHost;
    }

    public void setSettingHost(String settingHost) {
        this.settingHost = settingHost;
    }

    public String getSettingPort() {
        return settingPort;
    }

    public void setSettingPort(String settingPort) {
        this.settingPort = settingPort;
    }

    public String getSettingPassword() {
        return settingPassword;
    }

    public void setSettingPassword(String settingPassword) {
        this.settingPassword = settingPassword;
    }
}
