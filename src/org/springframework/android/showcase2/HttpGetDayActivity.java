package org.springframework.android.showcase2;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.achartengine.GraphicalView;
import android.view.ViewGroup.LayoutParams;

public class HttpGetDayActivity extends AbstractAsyncActivity {

    private TextView day_tView_Header;
    private android.widget.ListView day_lView_TableData;
    private LinearLayout day_chartlayout_ChartData;

    private Date selectedDate;
    private Date currentDate;
    private SimpleDateFormat ft;

    private GraphicalView mBarChartView;
    private SWBarChart swBarChart;

    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_layout);

        int selectedYearValue = 0;
        int selectedMonthValue = 0;
        int selectedDayValue = 0;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            selectedYearValue = extras.getInt("SELECTED_DATE_YEAR");
            selectedMonthValue = extras.getInt("SELECTED_DATE_MONTH");
            selectedDayValue = extras.getInt("SELECTED_DATE_DAY");
        }

        ft = new SimpleDateFormat ("yyyy-MM-dd");
        fm = getFragmentManager();

        day_lView_TableData = (ListView) findViewById(R.id.day_lView_TableData);
        day_chartlayout_ChartData = (LinearLayout) findViewById(R.id.chart);
        day_tView_Header = (TextView) findViewById(R.id.day_tView_Header);

        String testDate = "2013/02/13";

        Calendar cal = Calendar.getInstance();
        cal.set(selectedYearValue, selectedMonthValue, selectedDayValue);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

        try{
            String calend = formatter.format(cal.getTime());
            currentDate = formatter.parse(calend);

        }catch(Exception ex){
            Log.e(TAG, ex.getMessage());
        }


        selectedDate = currentDate;
        day_tView_Header.setText(new SimpleDateFormat("E, dd MMM yyyy").format(currentDate));
        new PostMessageTask().execute();

        /*
            Toggle between tabular and graph views
         */
        final Button buttonToggleView = (Button) findViewById(R.id.day_btn_viewtype);
        final LinearLayout subheaders = (LinearLayout) findViewById(R.id.subheaders);
        final TextView subhead2 = (TextView) findViewById(R.id.day_tView_Header2);
        final TextView subhead3 = (TextView) findViewById(R.id.day_tView_Header3);
        final TextView subhead4 = (TextView) findViewById(R.id.day_tView_Header4);

        buttonToggleView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(day_lView_TableData.getVisibility() == View.VISIBLE)
                {
                    displayGraphView();
                    buttonToggleView.setText("Detail");
                    subhead2.setVisibility(View.INVISIBLE);
                    subhead3.setVisibility(View.INVISIBLE);
                    subhead4.setVisibility(View.INVISIBLE);

                }
                else{
                    displayTabularDataView();
                    buttonToggleView.setText("Graph");
                    subhead2.setVisibility(View.VISIBLE);
                    subhead3.setVisibility(View.VISIBLE);
                    subhead4.setVisibility(View.VISIBLE);
                }
            }
        });


    }

    public void onClickLoad(View view){
        Intent i = new Intent("org.springframework.android.showcase2.HttpGetDayActivity");
        startActivity(i);
    }

    private void showResult(DayData result) {
        displayTabularDataView();


        swBarChart = new SWBarChart(this);

        SimpleDateFormat gregDateFormatter = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat slashDateFormatter = new SimpleDateFormat("yyyy/MM/dd");

        day_tView_Header.setText("Daily sales for " + gregDateFormatter.format(selectedDate));

        if (result != null) {
            String errorMsg = "Error Msg: " + result.error;
            String errorTitle = "Exception - NullPointer";

            if(result.error != null){
                if(result.error.equalsIgnoreCase("nopassphrase")){
                    errorMsg = "Please provide a password - Please use the back button after closing this message and then provide the password in the settings.";
                    errorTitle = "No password";
                    displayErrorModal(errorTitle, errorMsg, true);
                }

                if(result.error.equalsIgnoreCase("invalidpassphrase")){
                    errorMsg = "Provided password is incorrect - Please use the back button after closing this message and then correct the password in the settings.";
                    errorTitle = "Invalid password";
                    displayErrorModal(errorTitle, errorMsg, true);
                }

                if(result.error.equalsIgnoreCase("invaliddate")){
                    errorMsg = "The date chosen is invalid - Please use the back button after closing this message";
                    errorTitle = "Invalid date";
                    displayErrorModal(errorTitle, errorMsg, true);
                }

                if(result.error.equalsIgnoreCase("serverexception")){
                    errorMsg = "An exception occured on the server - Please use the back button after closing this message";
                    errorTitle = "General exception";
                    displayErrorModal(errorTitle, errorMsg, true);
                }

                if(result.error.equalsIgnoreCase("nodata")){
                    errorMsg = "No data available for the selected date";
                    errorTitle = "No data for selected date";
                    displayErrorModal(errorTitle, errorMsg, true);
                }
            }

            try{
                selectedDate = slashDateFormatter.parse(result.getTargetDate());
            }catch(Exception ex){


            }

            try{
                DayDataAdapter aa = new DayDataAdapter(this, R.layout.row_layout, buildBasicDataArrayForListView(result));
                aa.notifyDataSetChanged();
                day_lView_TableData.setAdapter(aa);

                float[] dblRandValues = new float[24];
                List<HourSalesItem> hsItems = null;

                try{
                    hsItems = result.getHourlySalesFigures();
                }catch(java.lang.NullPointerException npEx){
                    Log.e(TAG, npEx.getMessage());
                }

                for(int i = 0; i <= 23; i++){
                    HourSalesItem hsItem = hsItems.get(i);
                    dblRandValues[i] = hsItem.getTotalOfSales();
                }

                mBarChartView = swBarChart.initChart(dblRandValues);

                LinearLayout layoutBarGraph = (LinearLayout) findViewById(R.id.chart);
                layoutBarGraph.removeAllViews();
                layoutBarGraph.addView(mBarChartView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
                day_chartlayout_ChartData.refreshDrawableState();
                mBarChartView.repaint();
            }catch(Exception generalEx){

            }
        } else {
            // this.dataAsTable.setText("No data returned for date: " + ft.format(selectedDate));
            displayErrorModal("Server message", "No data returned for date: " + ft.format(selectedDate), true);

        }




    }

    private ArrayList<String[]> buildBasicDataArrayForListView(DayData dData){

        ArrayList<String[]> arr = new ArrayList<String[]>();


        String tmpVal = "R " + formatDecimal(dData.getTotalCash());
        arr.add(new String[]{"Cash", Integer.toString(dData.getNumberOfCashSales()), tmpVal});

        tmpVal = "R " + formatDecimal(dData.getTotalCard());
        arr.add(new String[]{"Card", Integer.toString(dData.getNumberOfCardSales()), tmpVal});

        tmpVal = "R " + formatDecimal(dData.getTotalCheque());
        arr.add(new String[]{"Cheque", Integer.toString(dData.getNumberOfChequeSales()), tmpVal});

        tmpVal = "R " + formatDecimal(dData.getTotalLoyalty());
        arr.add(new String[]{"Loyalty", Integer.toString(dData.getNumberOfLoyaltySales()), tmpVal});

        tmpVal = "R " + formatDecimal(dData.getTotalCharge());
        arr.add(new String[]{"Charge", Integer.toString(dData.getNumberOfChargeSales()), tmpVal});

        tmpVal = "R " + formatDecimal(dData.getTotalGrand());
        arr.add(new String[]{" ", " " , " "});
        arr.add(new String[]{"Grand Total", " ", tmpVal});
        return arr;
    }


    /**
     * Return a given float as a Decimal formatted string
     * @param number
     * @return String
     */
    private String formatDecimal(float number) {
        //return String.format("%10.2f", number);
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        formatter.setMinimumFractionDigits(2);
        symbols.setGroupingSeparator(',');
        return formatter.format(number);
    }

    private void displayErrorModal(String title, String message, boolean goToMainActivity){
        final MyAlertDialogFragment testDialog = new MyAlertDialogFragment(title, message, goToMainActivity);

        testDialog.setRetainInstance(true);

        testDialog.show(fm, "fragment_name");

    }

    private void displayErrorView(){
        day_lView_TableData.setVisibility(View.GONE);
        day_chartlayout_ChartData.setVisibility(View.GONE);
    }

    private void displayTabularDataView(){
        day_lView_TableData.setVisibility(View.VISIBLE);
        day_chartlayout_ChartData.setVisibility(View.GONE);
    }

    private void displayGraphView(){
        day_lView_TableData.setVisibility(View.GONE);
        day_chartlayout_ChartData.setVisibility(View.VISIBLE);
    }



    // ***************************************
    // Private class
    // ***************************************
    private class PostMessageTask extends AsyncTask<Void, Void, DayData> {
        private String abbreviation;
        private String hostUri;

        @Override
        protected void onPreExecute() {
            showLoadingProgressDialog();

            // Date format to be passed: yyyy-mm-dd
            abbreviation = ft.format(selectedDate);
        }

        @Override
        protected DayData doInBackground(Void... params) {
            String encodedQS = "";
            encodedQS = "/Sales/Day/" + abbreviation;

            // The URL for making the GET request
            hostUri = MainActivity.settingsHostUrl + ":" + MainActivity.settingsPort + encodedQS;

            MediaType mediaType = MediaType.APPLICATION_JSON;

            // Set the Accept header for "application/json" or "application/xml"
            HttpHeaders requestHeaders = new HttpHeaders();
            List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
            acceptableMediaTypes.add(mediaType);
            requestHeaders.setAccept(acceptableMediaTypes);

            // Add the pass phrase to the header
            requestHeaders.add("p", MainActivity.settingsPassPhrase);

            // Populate the headers in an HttpEntity object to use for the request
            HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);

            // Create a new RestTemplate instance
            RestTemplate restTemplate = new RestTemplate();

            if (mediaType.equals(MediaType.APPLICATION_JSON)) {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            } else if (mediaType.equals(MediaType.APPLICATION_XML)) {
                restTemplate.getMessageConverters().add(new SimpleXmlHttpMessageConverter());
            }

            try {
                // Perform the HTTP GET request
                ResponseEntity<DayData> responseEntity = restTemplate.exchange(hostUri, HttpMethod.GET, requestEntity, DayData.class, abbreviation);

                // Return the state from the ResponseEntity
                return responseEntity.getBody();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(DayData result) {
            dismissProgressDialog();
            showResult(result);
        }

    }


}


