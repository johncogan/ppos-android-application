package org.springframework.android.showcase2;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by johnadmin on 2013/09/02.
 */
public class MyAlertDialogFragment extends DialogFragment {
    private String title;
    private String message;
    private TextView tvError;
    private boolean goToMain;

    public MyAlertDialogFragment(String sTitle, String sMessage, boolean goToMainActivity){
        this.title = sTitle;
        this.message = sMessage;
        this.goToMain = goToMainActivity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle(this.title)
                .setMessage(this.message)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if(goToMain)
                                {
                                    Intent i = new Intent(getActivity(), org.springframework.android.showcase2.MainActivity.class);
                                    startActivity(i);
                                }
                            }
                        }
                )
                .create();
    }

}