package org.springframework.android.showcase2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * @author John Cogan
 */
@Root(name = "daydata")
public class DayData {

    @ElementList(inline = true)
    protected List<HourSalesItem> hourlySalesFigures;

    @Element
    protected String error;
    @Element
    protected String targetDate;
    @Element
    protected String previousDate;
    @Element
    protected String nextDate;
    @Element
    protected int numberOfCashSales;
    @Element
    protected float totalCash;
    @Element
    protected int numberOfCardSales;
    @Element
    protected float totalCard;
    @Element
    protected int numberOfChequeSales;
    @Element
    protected float totalCheque;
    @Element
    protected int numberOfLoyaltySales;
    @Element
    protected float totalLoyalty;
    @Element
    protected int numberOfChargeSales;
    @Element
    protected float totalCharge;
    @Element
    protected float totalGrand;

    public DayData(List<HourSalesItem> hourlySalesFigures, String error, String targetDate, String previousDate, String nextDate, int numberOfCashSales, float totalCash, int numberOfCardSales, float totalCard, int numberOfChequeSales, float totalCheque, int numberOfLoyaltySales, float totalLoyalty, int numberOfChargeSales, float totalCharge, float totalGrand) {
        this.error = error;
        this.hourlySalesFigures = hourlySalesFigures;
        this.targetDate = targetDate;
        this.previousDate = previousDate;
        this.nextDate = nextDate;
        this.numberOfCashSales = numberOfCashSales;
        this.totalCash = totalCash;
        this.numberOfCardSales = numberOfCardSales;
        this.totalCard = totalCard;
        this.numberOfChequeSales = numberOfChequeSales;
        this.totalCheque = totalCheque;
        this.numberOfLoyaltySales = numberOfLoyaltySales;
        this.totalLoyalty = totalLoyalty;
        this.numberOfChargeSales = numberOfChargeSales;
        this.totalCharge = totalCharge;
        this.totalGrand = totalGrand;
    }

    public DayData()
    {

    }

    public String getError() {
        return error;
    }

    public List<HourSalesItem> getHourlySalesFigures() {
        return hourlySalesFigures;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public String getPreviousDate() {
        return previousDate;
    }

    public String getNextDate() {
        return nextDate;
    }

    public int getNumberOfCashSales() {
        return numberOfCashSales;
    }

    public float getTotalCash() {
        return totalCash;
    }

    public int getNumberOfCardSales() {
        return numberOfCardSales;
    }

    public float getTotalCard() {
        return totalCard;
    }

    public int getNumberOfChequeSales() {
        return numberOfChequeSales;
    }

    public float getTotalCheque() {
        return totalCheque;
    }

    public int getNumberOfLoyaltySales() {
        return numberOfLoyaltySales;
    }

    public float getTotalLoyalty() {
        return totalLoyalty;
    }

    public int getNumberOfChargeSales() {
        return numberOfChargeSales;
    }

    public float getTotalCharge() {
        return totalCharge;
    }

    public float getTotalGrand() {
        return totalGrand;
    }

}
