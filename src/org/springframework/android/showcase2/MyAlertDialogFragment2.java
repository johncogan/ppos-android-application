package org.springframework.android.showcase2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by johnadmin on 2013/09/02.
 */
public class MyAlertDialogFragment2 extends DialogFragment {

    public static MyAlertDialogFragment2 newInstance(String title, String message) {
        MyAlertDialogFragment2 frag = new MyAlertDialogFragment2();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");

        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // ((FragmentAlertDialog)getActivity()).doPositiveClick();
                            }
                        }
                )
                .create();
    }
}