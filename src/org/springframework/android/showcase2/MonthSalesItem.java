package org.springframework.android.showcase2;

import org.simpleframework.xml.Element;

public class MonthSalesItem{
    @Element
    protected int month;
    @Element
    protected int numberOfSales;
    @Element
    protected float totalOfSales;

    public MonthSalesItem(int month, int numberOfSales, float totalOfSales) {
        this.month = month;
        this.numberOfSales = numberOfSales;
        this.totalOfSales = totalOfSales;
    }

    public MonthSalesItem(){

    }

    public int getMonth() {
        return this.month;
    }

    public void setDay(int month) {
        this.month = month;
    }

    public int getNumberOfSales() {
        return numberOfSales;
    }

    public void setNumberOfSales(int numberOfSales) {
        this.numberOfSales = numberOfSales;
    }

    public float getTotalOfSales() {
        return totalOfSales;
    }

    public void setTotalOfSales(float totalOfSales) {
        this.totalOfSales = totalOfSales;
    }
}