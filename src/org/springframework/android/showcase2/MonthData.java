package org.springframework.android.showcase2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * @author John Cogan
 */
@Root(name = "monthdata")
public class MonthData {

    @ElementList(inline = true)
    protected List<DaySalesItem> monthlySalesFigures;

    @Element
    protected String error;
    @Element
    protected String targetDate;
    @Element
    protected String previousDate;
    @Element
    protected String nextDate;
    @Element
    protected int numberOfCashSales;
    @Element
    protected float totalCash;
    @Element
    protected int numberOfCardSales;
    @Element
    protected float totalCard;
    @Element
    protected int numberOfChequeSales;
    @Element
    protected float totalCheque;
    @Element
    protected int numberOfLoyaltySales;
    @Element
    protected float totalLoyalty;
    @Element
    protected int numberOfChargeSales;
    @Element
    protected float totalCharge;
    @Element
    protected float totalGrand;
    @Element
    protected String monthName;
    @Element
    protected String numDaysInMonth;

    public MonthData(List<DaySalesItem> monthlySalesFigures, String error, String targetDate, String previousDate, String nextDate, int numberOfCashSales, float totalCash, int numberOfCardSales, float totalCard, int numberOfChequeSales, float totalCheque, int numberOfLoyaltySales, float totalLoyalty, int numberOfChargeSales, float totalCharge, float totalGrand, String monthName, String numDaysInMonth) {
        this.error = error;
        this.monthlySalesFigures = monthlySalesFigures;
        this.targetDate = targetDate;
        this.previousDate = previousDate;
        this.nextDate = nextDate;
        this.numberOfCashSales = numberOfCashSales;
        this.totalCash = totalCash;
        this.numberOfCardSales = numberOfCardSales;
        this.totalCard = totalCard;
        this.numberOfChequeSales = numberOfChequeSales;
        this.totalCheque = totalCheque;
        this.numberOfLoyaltySales = numberOfLoyaltySales;
        this.totalLoyalty = totalLoyalty;
        this.numberOfChargeSales = numberOfChargeSales;
        this.totalCharge = totalCharge;
        this.totalGrand = totalGrand;
        this.monthName = monthName;
        this.numDaysInMonth = numDaysInMonth;
    }

    public MonthData()
    {

    }

    public String getNumDaysInMonth() {
        return numDaysInMonth;
    }

    public String getMonthName() {
        return monthName;
    }

    public String getError() {
        return error;
    }

    public List<DaySalesItem> getMonthlySalesFigures() {
        return monthlySalesFigures;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public String getPreviousDate() {
        return previousDate;
    }

    public String getNextDate() {
        return nextDate;
    }

    public int getNumberOfCashSales() {
        return numberOfCashSales;
    }

    public float getTotalCash() {
        return totalCash;
    }

    public int getNumberOfCardSales() {
        return numberOfCardSales;
    }

    public float getTotalCard() {
        return totalCard;
    }

    public int getNumberOfChequeSales() {
        return numberOfChequeSales;
    }

    public float getTotalCheque() {
        return totalCheque;
    }

    public int getNumberOfLoyaltySales() {
        return numberOfLoyaltySales;
    }

    public float getTotalLoyalty() {
        return totalLoyalty;
    }

    public int getNumberOfChargeSales() {
        return numberOfChargeSales;
    }

    public float getTotalCharge() {
        return totalCharge;
    }

    public float getTotalGrand() {
        return totalGrand;
    }

}
